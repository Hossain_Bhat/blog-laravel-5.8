<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//public controller

Route::get('/', 'PublicController@index')->name('index');
Route::get('/about', 'PublicController@About')->name('About');
Route::get('/post/{id}', 'PublicController@singlePost')->name('singlePost');
Route::get('/contact', 'PublicController@contact')->name('contact');

//admin controler
Route::prefix('admin')->group(function(){
		Route::get('/dashboard', 'AdminController@Dashboard')->name('AdminDashboard');
		Route::get('/post', 'AdminController@Posts')->name('AdminPost');
		Route::get('/comment', 'AdminController@Comments')->name('AdminComments');
		Route::get('/user', 'AdminController@User')->name('AdminUser');
		Route::get('posts/{id}/edit', 'AdminController@editPost')->name('editPost');
		Route::post('posts/{id}/update', 'AdminController@updatePost')->name('updatePost');
		Route::post('posts/{id}/delete', 'AdminController@deletePost')->name('deletePost');

		Route::get('user/{id}/edit', 'AdminController@editUser')->name('editUser');
	    Route::post('user/{id}/update', 'AdminController@updateUser')->name('updateUser');
	    Route::post('user/{id}/delete', 'AdminController@deleteUser')->name('deleteUser');
	});
// author
Route::prefix('author')->group(function(){
		Route::get('/dashboard', 'AuthorController@Dashboard')->name('AuthorDashboard');
		Route::get('/posts', 'AuthorController@Posts')->name('AuthorPosts');
		Route::get('/comment', 'AuthorController@Components')->name('AuthorComments');
		Route::post('/newpost/create', 'AuthorController@createpost')->name('createpost');
		Route::get('/newpost', 'AuthorController@newpost')->name('newpost');
		Route::get('posts/{id}/edit', 'AuthorController@editPost')->name('editPost');
		Route::post('posts/{id}/update', 'AuthorController@updatePost')->name('updatePost');
		Route::post('posts/{id}/delete', 'AuthorController@deletePost')->name('deletePost');


	});
// user
Route::prefix('user')->group(function(){
		Route::get('/dashboard', 'UserController@Dashboard')->name('UserDashboard');
		Route::get('/comment', 'UserController@Comments')->name('UserComments');
		Route::get('/profile', 'UserController@UserProfile')->name('UserProfile');
		Route::post('/profile', 'UserController@UserProfilePost')->name('UserProfilePost');
		Route::post('/deleteComment/{id}', 'UserController@deleteComment')->name('deleteComment');
		Route::post('/newcomment', 'UserController@newcomment')->name('newcomment');
	});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
