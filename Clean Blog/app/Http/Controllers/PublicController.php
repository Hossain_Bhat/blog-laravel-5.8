<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PublicController extends Controller
{
    public function index(){
        $posts = Post::paginate(2);
    	return view('ForntEnd.index')->with(compact('posts'));
    }

    public function About(){
    	return view('ForntEnd.about');
    }

    public function singlePost($id){
        $post = Post::find($id);
        return view('ForntEnd.singlePost')->with(compact('post'));
    }

    public function contact(){
    	return view('ForntEnd.contact');
    }
}
