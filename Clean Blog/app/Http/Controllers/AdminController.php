<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\CheckRole;
use App\Http\Requests\PostCreate;
use App\Http\Requests\UserUpdate;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\User;


class AdminController extends Controller
{
	public function __construct(){
		return $this->Middleware('CheckRole:admin');
	}

    public function Dashboard(){
    	return view('BackEnd.admin.dashboard');
    }

    public function Posts(){

        $posts = Post::all();
    	return view('BackEnd.admin.Post')->with(compact('posts'));
    }

    public function Comments(){
    	return view('BackEnd.admin.Comments');
    }
    public function User(){
        $users = User::all();
    	return view('BackEnd.admin.User')->with(compact('users'));
    }

        public function editPost($id){

        $post= Post::where('id',$id)->first();
        return view('BackEnd.admin.editPost',  compact('post'));
    }

        public function updatePost(PostCreate $request, $id){

        $post= Post::where('id',$id)->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->user_id = Auth::id();
        $post->save();
        return back()->with('success','Post Updated Successfully');
       
    }

         public function deletePost($id){

        $post= Post::where('id',$id)->first();
        $post->delete();
        return back();
    }


    public function editUser($id){

        $user= User::where('id',$id)->first();
        return view('BackEnd.admin.editUser',  compact('user'));
    }
    public function updateUser(UserUpdate $request, $id){

        $user= User::where('id',$id)->first();
        $user->name = $request['name'];
        $user->email = $request['email'];
        if($request['author']==1){
            $user->author=true;
        }elseif($request['admin']==1){
            $user->admin=true;
        }
        $user->save();
        return back()->with('success','User Updated Successfully');
       
    }
    
     public function deleteUser($id){

        $post= User::where('id',$id)->first();
        $post->delete();
        return back();
    }
}
