<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\CheckRole;
use App\Http\Requests\PostCreate;
use Illuminate\Support\Facades\Auth;
use App\Post;

class AuthorController extends Controller
{

	public function __construct(){
		return $this->Middleware('CheckRole:author');
	}
   
	public function Dashboard(){
		return view('BackEnd.author.dashboard');
	}

	public function Posts(){
		return view('BackEnd.author.post');
	}

	public function Components(){
		return view('BackEnd.author.Components');
	}

	public function newpost(){
		return view('BackEnd.author.newpost');
	}
	public function createpost(PostCreate $request){
		$post = new Post();
		$post->title = $request['title'];
		$post->content = $request['content'];
		$post->user_id = Auth::id();
		$post->save();
		return back()->with('success','Post Created Successfully');
	}

	    public function editPost($id){
        $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
        return view('BackEnd.author.editPost',  compact('post'));
    }

        public function updatePost(PostCreate $request, $id){
        $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->user_id = Auth::id();
        $post->save();
        return back()->with('success','Post Updated Successfully');
       
    }

         public function deletePost($id){
        $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
        $post->delete();
        return back();
    }

	
}
