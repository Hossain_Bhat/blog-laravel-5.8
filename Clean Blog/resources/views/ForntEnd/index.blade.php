@extends('ForntEnd.layouts.master')
@section('title','Home')
@section('content')

  <header class="masthead"  style="background-image: url('{{'ForntEnd/img/home-bg.jpg'}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        @foreach($posts as $post)
        <div class="post-preview">
          <a href="{{Route('singlePost',$post->id)}}">
            <h2 class="post-title">
              {{$post->title}}
            </h2>
          </a>
          <p class="post-meta">Posted by
            <a href="#">{{$post->user->name}}</a>
            on {{date_format($post->created_at,'F d,Y')}} | {{$post->comments->count()}}</p>
        </div>
        <hr>
        @endforeach
        {{$posts->links()}}
        </div>
        <hr>
        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
        </div>
      </div>
    </div>
  </div>
@endsection