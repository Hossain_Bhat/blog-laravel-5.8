@extends('ForntEnd.layouts.master')
@section('title','Post')
@section('content')

  <header class="masthead" style="background-image: url('{{asset('ForntEnd/img/post-bg.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1>{{$post->title}}</h1>
            <span class="meta">Posted by
              <a href="#">{{$post->user->name}}</a>
              on {{date_format($post->created_at,'F d,Y')}}</span>
          </div>
        </div>
      </div>
    </div>
  </header>

    <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">   
          <p>{{$post->content}}</p>
        </div>
      </div>
      <div class="comment">
        <h2>Comment</h2>
        @foreach($post->comments as $comment)
        <hr>
        <p>{{$comment->content}}</p>
        <hr>
        <p><small>Posted By {{$comment->user->name}}, {{date_format($comment->created_at,'F d,Y')}}</small> </p>
        @endforeach
        @if(Auth::check())
        <form action="{{route('newcomment')}}" method="post">
          @csrf
        <div class="form-group">
          <textarea name="comment" rows="5" cols="30" class="form-control" placeholder="Make Comments"></textarea>
          <input type="hidden" name="post" value="{{$post->id}}">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Make Comment</button>
        </div>
       </form>
        @endif
      </div>
    </div>
  </article>


@endsection