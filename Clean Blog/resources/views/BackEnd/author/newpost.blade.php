@extends('layouts.admin')
@section('title','Author Create Post')
@section('content')
    <main class="app-content">
      <div class="app-title">
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Author</li>
          <li class="breadcrumb-item"><a href="#">Create Post</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Create Post</h3>
            <div class="tile-body">
              @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
                @endif
                
                 @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
              <form action="{{route('createpost')}}" method="post">
                @csrf
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input class="form-control" name="title" id="title" type="text" placeholder="Enter full name" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Description</label>
                  <textarea class="form-control" name="content" id="content" rows="4" placeholder="Enter your address" required></textarea>
                </div>
                <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>
            </div>
              </form>
            </div>

          </div>
        </div>

      </div>
    </main>

@endsection