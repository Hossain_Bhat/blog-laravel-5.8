    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">{{Auth::user()->name}}</p>
          <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="{{Route('UserDashboard')}}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">User</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{Route('UserDashboard')}}"><i class="icon fa fa-circle-o"></i> Deshboard</a></li>
            <li><a class="treeview-item" href="{{Route('UserComments')}}"><i class="icon fa fa-circle-o"></i> Components</a></li>
            
          </ul>
        </li>
        @if(Auth::user()->author == true)
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Author</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{Route('AuthorDashboard')}}"><i class="icon fa fa-circle-o"></i> Deshboard</a></li>
            <li><a class="treeview-item" href="{{Route('AuthorPosts')}}"><i class="icon fa fa-circle-o"></i> Post </a></li>
            <li><a class="treeview-item" href="{{Route('AuthorComments')}}"><i class="icon fa fa-circle-o"></i> Components</a></li>
            
          </ul>
        </li>
        @endif
        @if(Auth::user()->admin == true)
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Admin</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{Route('AdminDashboard')}}"><i class="icon fa fa-circle-o"></i> Deshboard</a></li>
            <li><a class="treeview-item" href="{{Route('AdminPost')}}"><i class="icon fa fa-circle-o"></i> Post </a></li>
            <li><a class="treeview-item" href="{{Route('AdminComments')}}"><i class="icon fa fa-circle-o"></i> Components</a></li>
            
          </ul>
        </li>
        @endif

        <li><a class="app-menu__item" href="{{Route('AdminUser')}}"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">User</span></a></li>

      </ul>