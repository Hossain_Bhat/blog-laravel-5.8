@extends('layouts.admin')
@section('title','User Deshboard')
@section('content')

    <main class="app-content">
      <div class="app-title">
        
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-6">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
              <h4>All Comments</h4>
              <p><b>{{ Auth::user()->comments()->count() }}</b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
            <div class="info">
              <h4>Comments</h4>
              <p><b>{{Auth::user()->comments->unique('user_id')->count()}}</b></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <h3 class="tile-title">Deshboard Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
              <!-- {!! $chart->container()!!} -->
            </div>
          </div>
        </div>
      </div>

    </main>
    <!-- {!! $chart->script() !!} -->
@endsection