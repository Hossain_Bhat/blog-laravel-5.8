@extends('layouts.admin')
@section('title','User Comments')
@section('content')

    <main class="app-content">
      <div class="app-title">
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">User</li>
          <li class="breadcrumb-item active"><a href="#">User Comment</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>#SL</th>
                    <th>ID</th>
                    <th>Post</th>
                    <th>Content</th>
                    <th>Created At</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <?php $data =1; ?>
                <tbody>
                	@foreach(Auth::user()->comments as $comment)
                  <tr>
                    <td>{{$data++}}</td>
                    <td>{{$comment->id}}</td>
                    <td><a  href="{{route('singlePost',$comment->id)}}">{{$comment->post_id}}</a></td>
                    <td>{{$comment->content}}</td>
                    <td>{{\carbon\carbon::parse($comment->created_at)->diffForHumans()}}</td>
                    <td>
                      <form action="{{route('deleteComment',$comment->id)}}" method="post" id="deletecomment-{{$comment->id}}">
                        @csrf
                        <button onclick="document.getElementById('deletecomment-{{$comment->id}}').submit()" class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </main>

@endsection